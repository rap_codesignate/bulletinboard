//
//  ABBHomeViewController.h
//  BulletinBoard
//
//  Created by raph on 12/9/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "ABBBaseViewController.h"
#import "ABBHomeView.h"
#import "ABBPostTableViewCell.h"
#import "AppDelegate.h"
#import "Post.h"
#import "Comment.h"

@interface ABBHomeViewController : ABBBaseViewController<ABBHomeViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) ABBHomeView *homeView;

@end
