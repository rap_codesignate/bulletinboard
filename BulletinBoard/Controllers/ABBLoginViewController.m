//
//  ABBLoginViewController.m
//  BulletinBoard
//
//  Created by raph on 12/9/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "ABBLoginViewController.h"

@interface ABBLoginViewController () {
    UITextField *activeField;
    NSArray *users;
    NSMutableArray *passwords;
    NSString *msg;
}

@end

@implementation ABBLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //for keyboard
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(hideKeyBoard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    //Load xib into view
    self.loginView = (ABBLoginView*) [self getCustomXibUsingXibName:@"LoginView"];
    
    self.loginView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    
    //Set Delegates
    self.loginView.delegate = self;
    self.loginView.usernameTextField.delegate = self;
    self.loginView.passwordTextField.delegate = self;
    
    [self registerForKeyboardNotifications];
    
    //init user and password arrays
    users = [[NSArray alloc] initWithObjects:@"user", @"user_test", nil];
    passwords = [[NSMutableArray alloc] initWithObjects:@"123456", @"qwerty", nil];
    
    //Add login view to view
    [self.view addSubview:self.loginView];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self.view resignFirstResponder];
    self.loginView.usernameTextField.text = @"";
    self.loginView.passwordTextField.text = @"";
    [self.loginView.usernameTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ABBLoginView Delegates
- (void)loginButtonPressed {
    NSLog(@"login button pressed");
    //[self performSegueWithIdentifier:@"LoginToHomeSegue" sender:self];
    
    NSString *inputUsername = self.loginView.usernameTextField.text;
    NSString *inputPassword = self.loginView.passwordTextField.text;
    
    //check if textfields are not empty
    if (![inputUsername isEqual:@""] && ![inputPassword isEqual:@""]) {
        //check if username and password match
        if ([users containsObject:inputUsername] && [passwords containsObject:inputPassword]) {
            NSInteger userIndex = [users indexOfObject:inputUsername];
            NSInteger passwordIndex = [passwords indexOfObject:inputPassword];
            
            //check if both arrays have the same index
            if (userIndex == passwordIndex) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:inputUsername forKey:@"username"];
                [self performSegueWithIdentifier:@"LoginToHomeSegue" sender:self];
            }
            else {
                //NSLog(@"login failed");
                msg = @"Login Failed";
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }
        else {
            msg = @"Incorrect Username or Password!";
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
    }
    else {
        msg = @"Please fill out textfields";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark - UITextFields Delegates
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    activeField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Keyboard
- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    // Get size of displayed keyboard
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // Compute visible active field
    CGRect visibleActiveFieldRect = CGRectMake(activeField.frame.origin.x, activeField.frame.origin.y + kbSize.height, activeField.frame.size.width, activeField.frame.size.height);
    
    // Adjust scroll view content size
    self.loginView.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + kbSize.height);
    
    // Scroll to visible active field
    [self.loginView.scrollView scrollRectToVisible:visibleActiveFieldRect animated:YES];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    // Reset the content size of the scroll view
    self.loginView.scrollView.contentSize = CGSizeMake(0.0, 0.0);
}

-(void)hideKeyBoard {
    [self.loginView.usernameTextField resignFirstResponder];
    [self.loginView.passwordTextField resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
