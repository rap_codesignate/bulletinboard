//
//  ABBHomeViewController.m
//  BulletinBoard
//
//  Created by raph on 12/9/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "ABBHomeViewController.h"

@interface ABBHomeViewController () {
    NSMutableArray *titles;
    NSMutableArray *posts;
    NSMutableArray *users;
}

@end

@implementation ABBHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Load xib into view
    self.homeView = (ABBHomeView*) [self getCustomXibUsingXibName:@"HomeView"];
    
    self.homeView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    
    //Set Delegates
    self.homeView.delegate = self;
    self.homeView.tableView.delegate = self;
    self.homeView.tableView.dataSource = self;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
//    if ([defaults objectForKey:@"isFirstRun"] == nil) {
//        NSLog(@"First Run");
//
//        [defaults setObject:@"YES" forKey:@"isFirstRun"];
//        NSLog(@"init arrays");
//        //init arrays
//        titles = [[NSMutableArray alloc] initWithObjects:@"title", @"another title", nil];
//        posts = [[NSMutableArray alloc] initWithObjects:@"post here", @"more posts here", nil];
//        users = [[NSMutableArray alloc] initWithObjects:@"user", @"user_test", nil];
//
//        //Store Arrays in NSUserdefaults
//        [defaults setObject:titles forKey:@"titles"];
//        [defaults setObject:posts forKey:@"posts"];
//        [defaults setObject:users forKey:@"users"];
//        [defaults synchronize];
//    }
    
    //init nsuserdefaults
    NSString *username = [defaults objectForKey:@"username"];
    
    //Set values of UIView elements
    self.homeView.userLabel.text = [username uppercaseString];
    if ([username isEqualToString:@"user"])
        self.homeView.userImage.image = [UIImage imageNamed:@"kobe"];
    else
        self.homeView.userImage.image = [UIImage imageNamed:@"mitsui"];
    
    //Add home view to view
    [self.view addSubview:self.homeView];
    
    //Hide Back Button
    self.navigationItem.hidesBackButton = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.homeView.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Logout Delegates
- (void)logoutPressed {
    NSLog(@"logout");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"username"];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - add post Delegates
- (void)addPostPressed {
    //NSLog(@"test");
    
    //init userdefaults
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *username = [defaults objectForKey:@"username"];
    //Retrieve users, posts, titles from userdefaults
//    NSMutableArray *mutableCopyTitles = [[defaults arrayForKey:@"titles"] mutableCopy];
//    NSMutableArray *mutableCopyPosts = [[defaults arrayForKey:@"posts"] mutableCopy];
//    NSMutableArray *mutableCopyUsers = [[defaults arrayForKey:@"users"] mutableCopy];
    
//    [mutableCopyTitles addObject:@"test title"];
//    [mutableCopyPosts addObject:@"test description"];
//    [mutableCopyUsers addObject:username];
    
    //Replace old instances
//    [defaults setObject:mutableCopyTitles forKey:@"titles"];
//    [defaults setObject:mutableCopyPosts forKey:@"posts"];
//    [defaults setObject:mutableCopyUsers forKey:@"users"];
    
//    [self.homeView.tableView reloadData];
    //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //[defaults setObject:@"add" forKey:@"open"];
    
    [self performSegueWithIdentifier:@"HomeToAddPostSegue" sender:self];
    
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //NSMutableArray *persistentTitles = [[defaults arrayForKey:@"titles"] mutableCopy];
    //NSLog(@"%lu",persistentTitles.count);
    //return persistentTitles.count;
    
    return [self getAllPost].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //Check which tableview ...
    if (tableView == self.homeView.tableView) {
        ABBPostTableViewCell *postCell = (ABBPostTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PostCell"];
        
        if (postCell == nil) {
            postCell = [[[NSBundle mainBundle] loadNibNamed:@"PostTableViewCell" owner:nil options:nil] objectAtIndex:0];
        }
        
        //init userdefaults
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        //Retrieve users, posts, titles from userdefaults
//        NSMutableArray *persistentTitles = [[defaults arrayForKey:@"titles"] mutableCopy];
//        NSMutableArray *persistentPosts = [[defaults arrayForKey:@"posts"] mutableCopy];
//        NSMutableArray *persistentUsers = [[defaults arrayForKey:@"users"] mutableCopy];
        
        Post *post = [[self getAllPost] objectAtIndex:indexPath.row];
        
        postCell.titleLabel.text = post.title;
        postCell.postLabel.text = post.body;
        postCell.userLabel.text = post.user;
        
        //Assign values to labels
//        postCell.titleLabel.text = [persistentTitles objectAtIndex:indexPath.row];
//        postCell.postLabel.text = [persistentPosts objectAtIndex:indexPath.row];
//        postCell.userLabel.text = [persistentUsers objectAtIndex:indexPath.row];
        
        return postCell;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSString *row = @(indexPath.row).stringValue;
    //init userdefaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //Retrieve users, posts, titles from userdefaults
    //[defaults setObject:row forKey:@"index"];
//    NSMutableArray *persistentTitles = [[defaults arrayForKey:@"titles"] mutableCopy];
//    NSMutableArray *persistentPosts = [[defaults arrayForKey:@"posts"] mutableCopy];
//    NSMutableArray *persistentUsers = [[defaults arrayForKey:@"users"] mutableCopy];
    
    //Assign values to labels
//    NSString *currentTitle = [persistentTitles objectAtIndex:indexPath.row];
//    NSString *currentPosts = [persistentPosts objectAtIndex:indexPath.row];
//    NSString *currentUsers = [persistentUsers objectAtIndex:indexPath.row];
    
//    [defaults setObject:currentTitle forKey:@"currentTitles"];
//    [defaults setObject:currentPosts forKey:@"currentPosts"];
//    [defaults setObject:currentUsers forKey:@"currentUsers"];
    
    NSString *row = @(indexPath.row).stringValue;
    [defaults setObject:row forKey:@"index"];
    
    [self performSegueWithIdentifier:@"HomeToPostSegue" sender:self];
}

- (NSArray*)getAllPost {
    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Post" inManagedObjectContext:ad.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [ad.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return result;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
