//
//  ABBBaseViewController.h
//  BulletinBoard
//
//  Created by raph on 12/9/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABBBaseViewController : UIViewController

- (UIView*)getCustomXibUsingXibName:(NSString*)xibName;

@end
