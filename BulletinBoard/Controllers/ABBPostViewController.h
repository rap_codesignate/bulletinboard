//
//  ABBPostViewController.h
//  BulletinBoard
//
//  Created by raph on 12/10/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "ABBBaseViewController.h"
#import "ABBPostView.h"
#import "AppDelegate.h"
#import  "Post.h"
#import "Comment.h"
#import "ABBCommentTableViewCell.h"

@interface ABBPostViewController : ABBBaseViewController<ABBPostViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>

@property (strong, nonatomic) ABBPostView *postView;

@end
