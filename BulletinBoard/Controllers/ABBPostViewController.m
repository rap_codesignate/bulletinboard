//
//  ABBPostViewController.m
//  BulletinBoard
//
//  Created by raph on 12/10/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "ABBPostViewController.h"

@interface ABBPostViewController () {
    UITextView *activeField;
}

@end

@implementation ABBPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //for keyboard
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(hideKeyBoard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    [self registerForKeyboardNotifications];
    
    //Load xib into view
    self.postView = (ABBPostView*) [self getCustomXibUsingXibName:@"PostView"];
    self.postView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    
    //Set Delegates
    self.postView.delegate = self;
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *title = [defaults objectForKey:@"currentTitles"];
//    NSString *post = [defaults objectForKey:@"currentPosts"];
//    NSString *user = [defaults objectForKey:@"currentUsers"];
    NSString *rowNum = [defaults objectForKey:@"index"];
    NSInteger row = [rowNum intValue];
    
    Post *post = [[self getAllPost]objectAtIndex:row];
    
    self.postView.titleLabel.text = post.title;
    self.postView.postLabel.text = post.body;
    self.postView.userLabel.text = [NSString stringWithFormat:@"%@ %@", @"Posted by: ", post.user];
    self.postView.commentTableView.delegate = self;
    self.postView.commentTableView.dataSource = self;

    
    //Add home view to view
    [self.view addSubview:self.postView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSArray*)getAllPost {
    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Post" inManagedObjectContext:ad.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [ad.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return result;
}

-(NSArray*)getAllComment {
    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSString *rowId = [[NSUserDefaults standardUserDefaults] objectForKey:@"index"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"comment_id", rowId];
    [fetchRequest setPredicate:predicate];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Comment" inManagedObjectContext:ad.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [ad.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return result;
}

#pragma mark - add comment delegate
- (void)addCommentPressed {
    NSString *comment = self.postView.commentTextView.text;
    NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    NSString *rowId = [[NSUserDefaults standardUserDefaults] objectForKey:@"index"];
    
    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSEntityDescription *entityComment = [NSEntityDescription entityForName:@"Comment" inManagedObjectContext:ad.managedObjectContext];
    NSManagedObject *newComment = [[NSManagedObject alloc] initWithEntity:entityComment insertIntoManagedObjectContext:ad.managedObjectContext];
    
    [newComment setValue:username forKey:@"comment_user"];
    [newComment setValue:comment forKey:@"comment_txt"];
    [newComment setValue:rowId forKey:@"comment_id"];
    
    NSError *error = nil;
    
    if (![newComment.managedObjectContext save:&error]) {
        NSLog(@"Unable to save managed object context.");
        //NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
    [self.postView.commentTableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //NSMutableArray *persistentTitles = [[defaults arrayForKey:@"titles"] mutableCopy];
    //NSLog(@"%lu",persistentTitles.count);
    //return persistentTitles.count;
    
    return [self getAllComment].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //Check which tableview ...
    if (tableView == self.postView.commentTableView) {
        ABBCommentTableViewCell *commentCell = (ABBCommentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
        
        if (commentCell == nil) {
            commentCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentTableViewCell" owner:nil options:nil] objectAtIndex:0];
        }
        
        //init userdefaults
        //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        //Retrieve users, posts, titles from userdefaults
        //        NSMutableArray *persistentTitles = [[defaults arrayForKey:@"titles"] mutableCopy];
        //        NSMutableArray *persistentPosts = [[defaults arrayForKey:@"posts"] mutableCopy];
        //        NSMutableArray *persistentUsers = [[defaults arrayForKey:@"users"] mutableCopy];
        
        Comment *comment = [[self getAllComment] objectAtIndex:indexPath.row];
        
        //commentCell.titleLabel.text = comment.title;
        commentCell.commentTextView.text = comment.comment_txt;
        commentCell.userLabel.text = comment.comment_user;
        
        //Assign values to labels
        //        postCell.titleLabel.text = [persistentTitles objectAtIndex:indexPath.row];
        //        postCell.postLabel.text = [persistentPosts objectAtIndex:indexPath.row];
        //        postCell.userLabel.text = [persistentUsers objectAtIndex:indexPath.row];
        
        return commentCell;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSString *row = @(indexPath.row).stringValue;
    //init userdefaults
    //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //Retrieve users, posts, titles from userdefaults
    //[defaults setObject:row forKey:@"index"];
    //    NSMutableArray *persistentTitles = [[defaults arrayForKey:@"titles"] mutableCopy];
    //    NSMutableArray *persistentPosts = [[defaults arrayForKey:@"posts"] mutableCopy];
    //    NSMutableArray *persistentUsers = [[defaults arrayForKey:@"users"] mutableCopy];
    
    //Assign values to labels
    //    NSString *currentTitle = [persistentTitles objectAtIndex:indexPath.row];
    //    NSString *currentPosts = [persistentPosts objectAtIndex:indexPath.row];
    //    NSString *currentUsers = [persistentUsers objectAtIndex:indexPath.row];
    
    //    [defaults setObject:currentTitle forKey:@"currentTitles"];
    //    [defaults setObject:currentPosts forKey:@"currentPosts"];
    //    [defaults setObject:currentUsers forKey:@"currentUsers"];
    
    //NSString *row = @(indexPath.row).stringValue;
    //[defaults setObject:row forKey:@"index"];
    
    //[self performSegueWithIdentifier:@"HomeToPostSegue" sender:self];
}

#pragma mark - UITextView Delegates
//- (void)textFieldDidBeginEditing:(UITextField *)textField {
    //activeField = textField;
//}

//- (void)textFieldDidEndEditing:(UITextField *)textField {
    //activeField = nil;
//}

//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [textField resignFirstResponder];
//    return YES;
//}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    activeField = textView;
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    activeField = nil;
}


#pragma mark - Keyboard
- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    // Get size of displayed keyboard
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // Compute visible active field
    CGRect visibleActiveFieldRect = CGRectMake(activeField.frame.origin.x, activeField.frame.origin.y + kbSize.height, activeField.frame.size.width, activeField.frame.size.height + kbSize.height);
    
    // Adjust scroll view content size
    self.postView.commentTextView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + kbSize.height);
    
    // Scroll to visible active field
    [self.postView.commentTextView scrollRectToVisible:visibleActiveFieldRect animated:YES];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    // Reset the content size of the scroll view
    self.postView.commentTextView.contentSize = CGSizeMake(0.0, 0.0);
}

-(void)hideKeyBoard {
    [self.postView.commentTextView resignFirstResponder];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
