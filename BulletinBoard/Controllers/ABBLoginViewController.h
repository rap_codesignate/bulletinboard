//
//  ABBLoginViewController.h
//  BulletinBoard
//
//  Created by raph on 12/9/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "ABBBaseViewController.h"
#import "ABBLoginView.h"

@interface ABBLoginViewController : ABBBaseViewController<ABBLoginViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) ABBLoginView *loginView;

@end
