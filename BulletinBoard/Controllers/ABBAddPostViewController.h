//
//  ABBAddPostViewController.h
//  BulletinBoard
//
//  Created by raph on 12/11/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "ABBBaseViewController.h"
#import "ABBAddPostView.h"
#import "AppDelegate.h"

@interface ABBAddPostViewController : ABBBaseViewController<ABBAddPostViewDelegate, UITextFieldDelegate, UITextViewDelegate>

@property (strong, nonatomic) ABBAddPostView *addpostView;

@end
