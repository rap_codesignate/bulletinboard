//
//  ABBAddPostViewController.m
//  BulletinBoard
//
//  Created by raph on 12/11/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "ABBAddPostViewController.h"

@interface ABBAddPostViewController () {
    UITextField *activeField;
    UITextView *activeView;
}

@end

@implementation ABBAddPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //for keyboard
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(hideKeyBoard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    [self registerForKeyboardNotifications];
    
    //Load xib into view
    self.addpostView = (ABBAddPostView*) [self getCustomXibUsingXibName:@"AddPostView"];
    self.addpostView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    
    //Set Delegates
    self.addpostView.delegate = self;
    self.addpostView.titleTextField.delegate = self;
    self.addpostView.postTextView.delegate = self;
    
    //Add addpost view to view
    [self.view addSubview:self.addpostView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - cancel post delegate
- (void)cancelPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - add post delegate
- (void)addPressed {
    //get input values
    NSString *inputTitle = self.addpostView.titleTextField.text;
    NSString *inputPost = self.addpostView.postTextView.text;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    //NSLog(username);
    
    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSEntityDescription *entityPost = [NSEntityDescription entityForName:@"Post" inManagedObjectContext:ad.managedObjectContext];
    NSManagedObject *newPost = [[NSManagedObject alloc] initWithEntity:entityPost insertIntoManagedObjectContext:ad.managedObjectContext];
    
    [newPost setValue:inputTitle forKey:@"title"];
    [newPost setValue:inputPost forKey:@"body"];
    [newPost setValue:username forKey:@"user"];
    
    NSError *error = nil;
    
    if (![newPost.managedObjectContext save:&error]) {
        NSLog(@"Unable to save managed object context.");
        //NSLog(@"%@, %@", error, error.localizedDescription);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextFields Delegates
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    activeField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    activeView = textView;
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    activeView = nil;
}


#pragma mark - Keyboard
- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    // Get size of displayed keyboard
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // Compute visible active field
    CGRect visibleActiveFieldRect = CGRectMake(activeField.frame.origin.x, activeField.frame.origin.y + kbSize.height, activeField.frame.size.width, activeField.frame.size.height);
//    CGRect visibleActiveViewRect = CGRectMake(activeView.frame.origin.x, activeView.frame.origin.y + kbSize.height, activeView.frame.size.width, activeView.frame.size.height);
    
    // Adjust scroll view content size
    self.addpostView.postTextView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + kbSize.height);
    
    // Scroll to visible active field
    [self.addpostView.postTextView scrollRectToVisible:visibleActiveFieldRect animated:YES];
//    [self.addpostView.postTextView scrollRectToVisible:visibleActiveViewRect animated:YES];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    // Reset the content size of the scroll view
    self.addpostView.postTextView.contentSize = CGSizeMake(0.0, 0.0);
}

-(void)hideKeyBoard {
    [self.addpostView.titleTextField resignFirstResponder];
    [self.addpostView.postTextView resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
