//
//  Comment.m
//  BulletinBoard
//
//  Created by raph on 12/12/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "Comment.h"
#import "Post.h"


@implementation Comment

@dynamic comment_id;
@dynamic comment_txt;
@dynamic comment_user;
@dynamic post;

@end
