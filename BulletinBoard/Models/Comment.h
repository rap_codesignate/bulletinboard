//
//  Comment.h
//  BulletinBoard
//
//  Created by raph on 12/12/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Post;

@interface Comment : NSManagedObject

@property (nonatomic, retain) NSString * comment_id;
@property (nonatomic, retain) NSString * comment_txt;
@property (nonatomic, retain) NSString * comment_user;
@property (nonatomic, retain) Post *post;

@end
