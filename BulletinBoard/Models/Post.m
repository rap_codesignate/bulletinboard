//
//  Post.m
//  BulletinBoard
//
//  Created by raph on 12/12/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "Post.h"
#import "Comment.h"


@implementation Post

@dynamic body;
@dynamic title;
@dynamic user;
@dynamic comment;

@end
