//
//  Post.h
//  BulletinBoard
//
//  Created by raph on 12/12/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Comment;

@interface Post : NSManagedObject

@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * user;
@property (nonatomic, retain) Comment *comment;

@end
