//
//  ABBPostTableViewCell.h
//  BulletinBoard
//
//  Created by raph on 12/10/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABBPostTableViewCell : UITableViewCell

//IBOutlets
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *postLabel;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;

@end
