//
//  ABBHomeView.m
//  BulletinBoard
//
//  Created by raph on 12/10/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "ABBHomeView.h"

@implementation ABBHomeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)logoutPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(logoutPressed)]) {
        [self.delegate logoutPressed];
    }
}

- (IBAction)addPostPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(addPostPressed)]) {
        [self.delegate addPostPressed];
    }
}
@end
