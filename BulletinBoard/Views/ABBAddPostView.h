//
//  ABBAddPostView.h
//  BulletinBoard
//
//  Created by raph on 12/11/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABBAddPostViewDelegate <NSObject>

@required

- (void)cancelPressed;
- (void)addPressed;

@end

@interface ABBAddPostView : UIView

//IBOutlets
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *postTextView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

//IBActions
- (IBAction)cancelPressed:(id)sender;
- (IBAction)addPressed:(id)sender;

@property (strong) id<ABBAddPostViewDelegate> delegate;

@end
