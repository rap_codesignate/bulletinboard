//
//  ABBHomeView.h
//  BulletinBoard
//
//  Created by raph on 12/10/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABBHomeViewDelegate <NSObject>

@required

- (void)logoutPressed;
- (void)addPostPressed;

@end

@interface ABBHomeView : UIView

//IBOutlets
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIButton *addPostButton;

//IBActions
- (IBAction)logoutPressed:(id)sender;
- (IBAction)addPostPressed:(id)sender;


@property (strong) id<ABBHomeViewDelegate> delegate;

@end
