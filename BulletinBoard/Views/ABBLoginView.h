//
//  ABBLoginView.h
//  BulletinBoard
//
//  Created by raph on 12/9/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABBLoginViewDelegate <NSObject>

@required

- (void)loginButtonPressed;

@end

@interface ABBLoginView : UIView

//IBOutlets
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

//IBActions
- (IBAction)loginButtonPressed:(id)sender;

@property (strong) id<ABBLoginViewDelegate> delegate;

@end
