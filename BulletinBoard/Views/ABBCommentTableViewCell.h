//
//  ABBCommentTableViewCell.h
//  BulletinBoard
//
//  Created by raph on 12/12/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABBCommentTableViewCell : UITableViewCell

//IBOutlets
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@end
