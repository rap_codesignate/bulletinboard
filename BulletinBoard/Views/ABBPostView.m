//
//  ABBPostView.m
//  BulletinBoard
//
//  Created by raph on 12/10/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "ABBPostView.h"

@implementation ABBPostView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (IBAction)addCommentPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(addCommentPressed)]) {
        [self.delegate addCommentPressed];
    }
}
@end
