//
//  ABBAddPostView.m
//  BulletinBoard
//
//  Created by raph on 12/11/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import "ABBAddPostView.h"

@implementation ABBAddPostView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)cancelPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelPressed)]) {
        [self.delegate cancelPressed];
    }
}

- (IBAction)addPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(addPressed)]) {
        [self.delegate addPressed];
    }
}
@end
