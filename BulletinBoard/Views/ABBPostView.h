//
//  ABBPostView.h
//  BulletinBoard
//
//  Created by raph on 12/10/14.
//  Copyright (c) 2014 raph. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABBPostViewDelegate <NSObject>

@required

- (void)addCommentPressed;

@end

@interface ABBPostView : UIView

//IBOutlets
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UIButton *addComment;
@property (weak, nonatomic) IBOutlet UITextView *postLabel;
@property (weak, nonatomic) IBOutlet UITableView *commentTableView;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

//IBActions
- (IBAction)addCommentPressed:(id)sender;


@property (strong) id<ABBPostViewDelegate> delegate;

@end
